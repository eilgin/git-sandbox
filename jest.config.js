module.exports = {
  verbose: true,
  testEnvironment: 'node',
  testMatch: ['**/!(node_modules)/**/__tests__/**.test.js'],
  testPathIgnorePatterns: ['<rootDir>/src/index.js'],
};
