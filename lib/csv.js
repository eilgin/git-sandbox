import { createReadStream } from 'fs';
import { createInterface } from 'readline';
import parse from 'csv-parse';

const defaultOpts = { delimiter: ';', fs: {} };

// read the entire file for line breaks (we assume it's a plain-text file)
function countLines(filepath, fsOpts) {
    return new Promise((resolve) => {
        let lineNumber = 0;
        const stream = createReadStream(filepath, fsOpts);
        stream.on('error', (err) => console.error(err.message));

        const rl = createInterface({
            input: stream,
            crlfDelay: Infinity,
        });

        rl.on('line', () => lineNumber++);
        rl.on('close', () => resolve(lineNumber));
    });
}

export async function* readCSV(filepath, options = defaultOpts) {
    const { fs: fsOpts, ...csvOpts } = options;
    const totalLines = await countLines(filepath, fsOpts);
    const stream = createReadStream(filepath, fsOpts);
    const parser = stream.pipe(parse(csvOpts));

    for await (const record of parser) {
        const { lines } = parser.info;
        // note: lines is a "1-based" index (see https://csv.js.org/parse/info)
        const line = lines - 1;
        const progress = line / totalLines;

        yield { record, line, progress };
    }
}
