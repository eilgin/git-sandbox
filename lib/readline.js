import fs from 'fs';
import readline from 'readline';

// read the entire file for line breaks (we assume it's a plain-text file)
function countLines(filepath, options) {
    return new Promise((resolve) => {
        let lineNumber = 0;
        const stream = fs.createReadStream(filepath, options);
        stream.on('error', (err) => console.error(err.message));

        const rl = readline.createInterface({
            input: stream,
            crlfDelay: Infinity,
        });

        rl.on('line', () => lineNumber++);
        rl.on('close', () => resolve(lineNumber));
    });
}

export async function* readLines(filepath, options) {
    let line = 0;
    const totalLines = await countLines(filepath, options);

    const stream = fs.createReadStream(filepath, options);
    const rl = readline.createInterface({
        input: stream,
        crlfDelay: Infinity,
    });

    // we can't use readline as an async iterator until Node.js 11.4.0
    // see https://github.com/nodejs/node/blob/master/doc/changelogs/CHANGELOG_V11.md#2018-12-07-version-1140-current-bridgear
    for await (const record of rl) {
        line += 1;
        const progress = line / totalLines;

        yield { record, line, progress };
    }
}
