import fs from 'fs';
import parse from 'csv-parse';

const defaultOpts = { delimiter: ';', fs: {} };

export function parseCSVFromFile(filePath, options = defaultOpts) {
    return new Promise((resolve, reject) => {
        const { fs: fsOptions, ...rest } = options;
        const readStr = fs.createReadStream(filePath, fsOptions);
        const csv = [];
        const parser = parse(rest, (err, data) => {
            if (err) {
                reject(err);
                return;
            }

            csv.push(data);
        });
        readStr.on('error', (err) => {
            reject(err);
        });
        readStr.on('close', () => {
            resolve(csv[0]);
        });
        readStr.pipe(parser);
    });
}
