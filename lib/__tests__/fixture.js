import { writeFile, unlink } from 'fs';
import { resolve } from 'path';
import { promisify } from 'util';

const asyncWrite = promisify(writeFile);
const asyncUnlink = promisify(unlink);

export async function generate(filepath, line, occurence, lineBreak = '\r\n') {
    let content = '';
    for (let i = 0; i < occurence; i++) {
        content += `${line}${lineBreak}`;
    }

    await asyncWrite(filepath, content);
}

export async function destroy(filepath) {
    await asyncUnlink(filepath);
}
