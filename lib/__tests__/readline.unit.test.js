import { readLines } from '../readline';
import { generate, destroy } from './fixture';

describe('text file reader', () => {
    const line = 'So Long, and Thanks for All the Fish!';
    const occurence = 42;
    const filepath = `${__dirname}/${occurence}.txt`;

    beforeEach(async () => await generate(filepath, line, occurence, '\n'));
    afterEach(async () => await destroy(filepath));

    it.skip('should work', async () => {
        const rl = readLines(fixture);

        let count = 0;
        for await (const value of rl) {
            const { record, line } = value;

            expect(record).toBe('So Long, and Thanks for All the Fish!');
            count = line;
        }

        expect(count).toBe(occurence);
    });
});
