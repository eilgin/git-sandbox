import { readCSV } from '../csv';
import { generate, destroy } from './fixture';

describe('CSV reader', () => {
    const line = Array(8).fill('accident').join(',');
    const occurence = 100;
    const filepath = `${__dirname}/${occurence}.csv`;

    beforeEach(async () => await generate(filepath, line, occurence));
    afterEach(async () => await destroy(filepath));

    it('should work', async () => {
        const csvLines = readCSV(filepath, { delimiter: ',' });

        let count = 0;
        for await (const value of csvLines) {
            const { record, line } = value;

            expect(record).toEqual([
                'accident',
                'accident',
                'accident',
                'accident',
                'accident',
                'accident',
                'accident',
                'accident',
            ]);

            count = line;
        }

        expect(count).toBe(occurence);
    });
});
