import { access, constants, createWriteStream } from 'fs';
import { resolve } from 'path';
import { promisify } from 'util';
import { finished } from 'stream';
import { Bench } from './utils';

import { parseCSVFromFile } from '../oldCSV';
import { readCSV } from '../csv';

const asyncAccess = promisify(access);

function filepath(lines, prefix = 'gen-output') {
    return resolve(__dirname, `./${prefix}-${lines}.csv`);
}

function draining(emitter) {
    return new Promise((resolve) => {
        emitter.once('drain', () => {
            resolve();
        });
    });
}

async function generateCSV(lines = 10, overwrite = false) {
    const fp = filepath(lines);

    const isReadable = await asyncAccess(fp, constants.R_OK)
        .then(() => true)
        .catch(() => false);

    if (!overwrite && isReadable) {
        console.info(`file '${fp}' already exists. Skipping ...`);
        return;
    }

    const wstream = createWriteStream(fp);
    let progressThreshold = 0;
    const progressIncrement = 10.0;

    for (let i = 0; i < lines; i++) {
        // add 16 columns times a word 8 chars long
        const line = Array(16).fill('accident').join(';');

        if (!wstream.write(`${line}\r\n`)) {
            // handle backpressure
            await draining(wstream);
        }

        const progress = parseInt((i / lines) * 100);
        if (Math.abs(progress - progressThreshold) < Number.EPSILON) {
            console.info(`writing '${fp}' ... ${progress}%`);
            progressThreshold += progressIncrement;
        }
    }

    wstream.end();
    await finished(wstream);
}

async function main() {
    // 1. generate CSV files
    // we used an exponential rate to generate multiple orders of file size
    const lineSizes = [
        100,
        1000,
        10000,
        Math.round(Math.exp(11)),
        Math.round(Math.exp(12)),
        Math.round(Math.exp(13)),
        Math.round(Math.exp(14)),
        Math.round(Math.exp(14.2)),
        Math.round(Math.exp(14.5)),
        // XXX beyond that threshold the current method will crash the node process!
        //Math.round(Math.exp(15)),
        //Math.round(Math.exp(16)),
        //Math.round(Math.exp(17)),
    ];

    // this may not be the best approach as we're writing all files in parallel
    await Promise.all(lineSizes.map((lines) => generateCSV(lines)));

    // 2. benchmark parsing methods
    const bench = new Bench();

    // a. the current method
    for (const lines of lineSizes) {
        const fp = filepath(lines);

        await bench.run('current-method', lines, async () => {
            const csv = await parseCSVFromFile(fp);
            console.info(`old> read lines: ${csv.length}`);
        });
    }

    // b. the new method
    for (const lines of lineSizes) {
        const fp = filepath(lines);

        await bench.run('new-method', lines, async () => {
            const parsedCSV = readCSV(fp);

            let readLines = 0;
            for await (const { line } of parsedCSV) {
                readLines = line;
            }

            console.info(`new> read lines: ${readLines}`);
        });
    }

    await bench.save();
}

main();
