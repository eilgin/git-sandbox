/*
 * This bench tool doesn't track memory usage because it would require to launch `node --expose-gc`
 * in order to have a useful `rss` value from `process.memoryUsage()`
 */
import { writeFile } from 'fs';
import { resolve } from 'path';
import { promisify } from 'util';

const asyncWriteFile = promisify(writeFile);

function toSec(ns) {
    const SECOND_IN_NS = 1 * 1000 ** 3;
    // keep the precision up to the millisecond
    const elapsedSeconds = (Number(ns) / SECOND_IN_NS).toFixed(3);

    return Number(elapsedSeconds);
}

export class Bench {
    constructor() {
        this.results = new Map();
        this.names = new Set();
    }

    async run(name, variant, cb) {
        // remember that we should only test against one "dimension" at a time
        console.info(`executing '${name}' (variant=${variant}) ...`);

        const _saveResult = (elapsedTime) => {
            const hasVariant = this.results.has(variant);
            const variantResults = hasVariant ? this.results.get(variant) : new Map();

            // we can store multiple durations for each variant (e.g. comparing differents implementations)
            variantResults.set(name, elapsedTime);

            // we also store used names
            this.names.add(name);

            this.results.set(variant, variantResults);
        };

        const start = process.hrtime.bigint();

        try {
            // we could run promise-based functions
            await cb();
        } catch (err) {
            _saveResult(-1);

            const ns = process.hrtime.bigint() - start;
            console.error(
                `an error occured while executing '${name}' (variant=${variant}) after ${toSec(
                    ns,
                )} sec. Reason:`,
                err.message,
            );

            return;
        }

        const ns = process.hrtime.bigint() - start;
        _saveResult(ns);

        console.info(`it took ${toSec(ns)} sec to run '${name}' (variant=${variant})\n`);
    }

    async save(useTimestamp = false, prefix = 'benchmark') {
        const br = (expr) => `${expr}\r\n`;

        // retrieve names to compose the filename
        let filename = `${prefix}-${Array.from(this.names).join('-vs-')}`;

        if (useTimestamp) {
            filename += `-${Date.now()}`;
        }

        const filepath = resolve(__dirname, `${filename}.csv`);
        const delim = ';';

        // here we assume that the different durations stored for each variant are always in the same order
        // e.g. variant=10 ; [name=foo,value=24], [name=bar,value=42]
        //      variant=20 ; [name=foo,value=43], [name=bar,value=86]...
        const cols = new Set(['variable metric']);

        let body = '';
        for (const [variant, res] of this.results) {
            let line = [variant];

            for (const [name, elapsedTime] of res) {
                cols.add(`${name} (ns)`);
                line.push(elapsedTime);
            }

            body += br(line.join(delim));
        }

        let head = br(Array.from(cols).join(delim));
        let content = `${head}${body}`;

        await asyncWriteFile(filepath, content);
        console.info(`saved file to '${filepath}'`);
    }
}
