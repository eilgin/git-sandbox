import { readCSV } from './lib/csv';
import { readLines } from './lib/readline';

export { readCSV, readLines };
